# SPlotting

This tutorial shows a simple example of background subtraction using the SPlot method. It uses the same fit as the fitting example. This runs the fit, computes the weights, saves the weights along with the original data to a tuple, and draws the background subtracted version of one of the other variables. See the annotated code in python/runme.py

## Running
  * source setup.sh # This is if you are running on the imperial machines
  * python python/runme.py

